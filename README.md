# Những sai lầm khi ứng dụng CRM vào tự động hóa

Về văn hoá kinh doanh tại Việt Nam, nhân viên kinh doanh thường có xu hướng giữ bí mật về thông tin khách hàng mà mình phụ trách. Nhiều nhân viên Sales chưa có tư tưởng gắn bó lâu dài với DN.